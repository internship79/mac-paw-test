<?php

namespace App\Notifications\Telegram;

use Illuminate\Bus\Queueable;
use NotificationChannels\Telegram\TelegramMessage;

class NeoSyncNotification extends TelegramNotification
{
    use Queueable;

    /**
     * @param $notifiable
     * @return TelegramMessage
     */
    public function toTelegram($notifiable): TelegramMessage
    {
        return TelegramMessage::create()
            ->content("NearEarthsObjects table has been synchronized with NASA")
            ->button('View Details', 'https://laravel-notification-channels.com/telegram/');
    }
}
