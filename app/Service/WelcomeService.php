<?php

namespace App\Service;

class WelcomeService
{
    /**
     * @return string
     */
    public function welcome(): string
    {
        return "MacPaw Internship 2022!";
    }
}
