<?php

namespace App\Service;

use App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\NeoFeedData;
use App\Infrastructure\IntegrationApi\Nasa\NeoFeedIntegration;
use App\Jobs\Neo\SendTelegram;
use App\Notifications\Telegram\NeoSyncNotification;
use App\Notifications\Telegram\NeoTruncateNotification;
use App\Repositories\Neo\NeoRepositoryInterface;
use Illuminate\Support\Collection;

class NeoService
{
    /**
     * @param NeoRepositoryInterface $neoRepository
     * @param NeoFeedIntegration $neoFeedIntegration
     */
    public function __construct(
        private readonly NeoRepositoryInterface $neoRepository,
        private readonly NeoFeedIntegration $neoFeedIntegration
    ) {}

    /**
     * @return Collection
     */
    public function getAllHazardous(): Collection
    {
        return $this->neoRepository->getByHazardous(true);
    }

    /**
     * @param string $hazardous
     * @return Collection
     */
    public function getFastest(string $hazardous): Collection
    {
        return $this->neoRepository->getByHazardous($hazardous ==='true')
            ->sortByDesc('speed');
    }

    /**
     * @return void
     */
    public function truncate(): void
    {
        $this->neoRepository->truncate();

        SendTelegram::dispatch(new NeoTruncateNotification());
    }

    /**
     * @return void
     */
    public function syncWithNasaNeoFeed(): void
    {
        $this->neoRepository->upsert(array_map(fn(NeoFeedData $data) =>
        $data->arrayForDb(), $this->neoFeedIntegration->getNeoFeed()));

        SendTelegram::dispatch(new NeoSyncNotification());
    }
}
