<?php

namespace App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData;

use App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData\CloseApproachData;
use App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData\EstimatedDiameterData;

class NeoFeedData
{
    /**
     * @param array $links
     * @param int $id
     * @param int $neoReferenceId
     * @param string $name
     * @param string $nasaJplUrl
     * @param float $absoluteMagnitudeH
     * @param EstimatedDiameterData $estimatedDiameter
     * @param bool $isPotentiallyHazardousAsteroid
     * @param array $closeApproachData
     * @param bool $isSentryObject
     */
    public function __construct (
        public readonly array $links,
        public readonly int $id,
        public readonly int $neoReferenceId,
        public readonly string $name,
        public readonly string $nasaJplUrl,
        public readonly float $absoluteMagnitudeH,
        public readonly EstimatedDiameterData $estimatedDiameter,
        public readonly bool $isPotentiallyHazardousAsteroid,
        public readonly array $closeApproachData,
        public readonly bool $isSentryObject
    ) {}

    /**
     * @param array $data
     * @return static
     */
    public static function instanceFromArray(array $data): self
    {
        return new self(
            $data['links'],
            $data['id'],
            $data['neo_reference_id'],
            $data['name'],
            $data['nasa_jpl_url'],
            $data['absolute_magnitude_h'],
            EstimatedDiameterData::instanceFromArray($data['estimated_diameter']),
            $data['is_potentially_hazardous_asteroid'],
            array_map(fn(array $data) => CloseApproachData::instanceFromArray($data), $data['close_approach_data']),
            $data['is_sentry_object']
        );
    }

    /**
     * @return array
     */
    public function arrayForDb(): array
    {
        return [
            'referenced' => $this->neoReferenceId,
            'date' => $this->getFirstCloseApproachData()->closeApproachDate->toDate(),
            'name' => $this->name,
            'speed' => $this->getFirstCloseApproachData()->relativeVelocity->kilometersPerHour,
            'is_hazardous' => $this->isPotentiallyHazardousAsteroid
        ];
    }

    /**
     * @return CloseApproachData
     */
    public function getFirstCloseApproachData(): CloseApproachData
    {
        return current($this->closeApproachData);
    }

}
