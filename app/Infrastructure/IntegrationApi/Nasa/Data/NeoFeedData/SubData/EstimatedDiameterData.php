<?php

namespace App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData;

class EstimatedDiameterData
{
    /**
     * @param array $kilometers
     * @param array $meters
     * @param array $miles
     * @param array $feet
     */
    public function __construct(
        public readonly array $kilometers,
        public readonly array $meters,
        public readonly array $miles,
        public readonly array $feet
    ){}

    /**
     * @param array $data
     * @return static
     */
    public static function instanceFromArray(array $data): self
    {
        return new self(
            $data['kilometers'],
            $data['meters'],
            $data['miles'],
            $data['feet']
        );
    }
}
