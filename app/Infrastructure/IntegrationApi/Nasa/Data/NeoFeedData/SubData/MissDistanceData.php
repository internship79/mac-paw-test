<?php

namespace App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData;

class MissDistanceData
{
    /**
     * @param float $astronomical
     * @param float $lunar
     * @param float $kilometers
     * @param float $miles
     */
    public function __construct (
        public readonly float $astronomical,
        public readonly float $lunar,
        public readonly float $kilometers,
        public readonly float $miles
    ) {}

    /**
     * @param array $data
     * @return static
     */
    public static function instanceFromArray(array $data): self
    {
        return new self(
            $data['astronomical'],
            $data['lunar'],
            $data['kilometers'],
            $data['miles']
        );
    }
}
