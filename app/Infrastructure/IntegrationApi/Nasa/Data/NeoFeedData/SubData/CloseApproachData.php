<?php

namespace App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData;

use Carbon\Carbon;

class CloseApproachData
{
    /**
     * @param Carbon $closeApproachDate
     * @param Carbon $closeApproachDateFull
     * @param int $epochDateCloseApproach
     * @param RelativeVelocityData $relativeVelocity
     * @param MissDistanceData $missDistance
     * @param string $orbitingBody
     */
    public function __construct (
        public readonly Carbon $closeApproachDate,
        public readonly Carbon $closeApproachDateFull,
        public readonly int $epochDateCloseApproach,
        public readonly RelativeVelocityData $relativeVelocity,
        public readonly MissDistanceData $missDistance,
        public readonly string $orbitingBody
    ) {}

    /**
     * @param array $data
     * @return static
     */
    public static function instanceFromArray(array $data): self
    {
        return new self(
            Carbon::make($data['close_approach_date']),
            Carbon::make($data['close_approach_date_full']),
            $data['epoch_date_close_approach'],
            RelativeVelocityData::instanceFromArray($data['relative_velocity']),
            MissDistanceData::instanceFromArray($data['miss_distance']),
            $data['orbiting_body']
        );
    }
}
