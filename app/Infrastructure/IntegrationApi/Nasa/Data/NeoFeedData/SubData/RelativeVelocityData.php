<?php

namespace App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\SubData;

class RelativeVelocityData
{
    /**
     * @param float $kilometersPerSecond
     * @param float $kilometersPerHour
     * @param float $milesPerHour
     */
    public function __construct(
        public readonly float $kilometersPerSecond,
        public readonly float $kilometersPerHour,
        public readonly float $milesPerHour
    ) {}

    /**
     * @param array $data
     * @return static
     */
    public static function instanceFromArray(array $data): self
    {
        return new self(
            $data['kilometers_per_second'],
            $data['kilometers_per_hour'],
            $data['miles_per_hour']
        );
    }
}
