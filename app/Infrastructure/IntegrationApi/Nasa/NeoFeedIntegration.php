<?php

namespace App\Infrastructure\IntegrationApi\Nasa;

use App\Infrastructure\IntegrationApi\Nasa\Data\NeoFeedData\NeoFeedData;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;

class NeoFeedIntegration
{
    /**
     * @return array
     */
    public function getNeoFeed(): array
    {
        $response = Http::get($this->getFullUrl('/neo/rest/v1/feed'), [
            'start_date' => Carbon::now('utc')->toDateString(),
            'end_date' => Carbon::now('utc')->subDays(2)->toDateString(),
            'api_key' => Config::get('services.nasa.api_key'),
        ])['near_earth_objects'];

        $neos = [];
        foreach ($response as $day => $neosPerDay) {
            foreach ($neosPerDay as $neo) {
                $neos[] = $neo;
            }
        }

        return array_map(fn(array $data) => NeoFeedData::instanceFromArray($data), $neos);
    }

    /**
     * @param string $path
     * @return string
     */
    private function getFullUrl(string $path): string
    {
        return Config::get('services.nasa.base_url') . $path;
    }
}
