<?php

namespace App\Console\Commands\Neo;

use App\Service\NeoService;
use Illuminate\Console\Command;

class TruncateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'neo:truncate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Truncate "near_earth_objects" table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(NeoService $service): int
    {
        if($this->confirm('Do you wish to truncate near_earth_objects_table?')) {
            $service->truncate();
            return self::SUCCESS;
        }
        return self::INVALID;
    }
}
