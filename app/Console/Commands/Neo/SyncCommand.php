<?php

namespace App\Console\Commands\Neo;

use App\Service\NeoService;
use Illuminate\Console\Command;

class SyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'neo:sync
                            {--truncate : Whether the table should be truncated first?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync near_earth_objects_table with NASA';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(NeoService $service): int
    {
        if($this->option('truncate')) {
            $this->call(TruncateCommand::class);
        }
        $service->syncWithNasaNeoFeed();
        return self::SUCCESS;
    }
}
