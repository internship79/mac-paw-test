<?php

namespace App\Repositories\Neo;

use Illuminate\Support\Collection;

interface NeoRepositoryInterface
{
    /**
     * @param bool $hazardous
     * @return Collection
     */
    public function getByHazardous(bool $hazardous): Collection;

    /**
     * @return void
     */
    public function truncate(): void;

    public function upsert(array $data);
}
