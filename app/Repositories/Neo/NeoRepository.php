<?php

namespace App\Repositories\Neo;

use App\Models\NearEarthObject;
use App\Repositories\BaseRepository;
use Illuminate\Support\Collection;

final class NeoRepository extends BaseRepository implements NeoRepositoryInterface
{
    public function __construct(NearEarthObject $model)
    {
        parent::__construct($model);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getByHazardous(bool $hazardous): Collection
    {
        return $this->model->newModelQuery()
            ->where("is_hazardous", $hazardous)
            ->toBase()
            ->get();
    }

    /**
     * @return void
     */
    public function truncate(): void
    {
        $this->model->newModelQuery()->truncate();
    }

    /**
     * @param array $data
     * @return int
     */
    public function upsert(array $data): int
    {
        return $this->model->newModelQuery()->upsert($data, 'reference');
    }
}
