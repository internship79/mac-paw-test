<?php

namespace App\Repositories\Neo;

use App\Repositories\CacheDecorator;
use Illuminate\Cache\Repository;
use Illuminate\Support\Collection;

class NeoRepositoryCaching extends CacheDecorator implements NeoRepositoryInterface
{
    /**
     * @param Repository $cache
     * @param NeoRepository $repository
     */
    public function __construct(
        protected readonly Repository $cache,
        protected readonly NeoRepository $repository
    ){}

    public function getByHazardous(bool $hazardous): Collection
    {
        return $this->getTaggedCache()->remember( __METHOD__ . $hazardous, self::CACHE_DURATION,
            fn() => $this->repository->getByHazardous($hazardous));
    }

    /**
     * @return void
     */
    public function truncate(): void
    {
        $this->getTaggedCache()->flush();
        $this->repository->truncate();
    }

    /**
     * @param array $data
     * @return void
     */
    public function upsert(array $data)
    {
        $this->getTaggedCache()->flush();
        $this->repository->upsert($data);
    }

    /**
     * @return Repository
     */
    protected function getCache(): Repository
    {
        return $this->cache;
    }

    /**
     * @return string
     */
    protected function getCacheTag(): string
    {
        return 'neo';
    }
}
