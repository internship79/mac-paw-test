<?php

namespace App\Repositories;

use Illuminate\Cache\Repository;
use Illuminate\Cache\TaggedCache;

abstract class CacheDecorator
{
    protected const CACHE_DURATION = 60;

    /**
     * @return bool
     */
    public function flush(): bool
    {
        return $this->getTaggedCache()->flush();
    }

    /**
     * @return TaggedCache
     */
    protected function getTaggedCache(): TaggedCache
    {
        return $this->getCache()->tags($this->getCacheTag());
    }

    /**
     * @return string
     */
    abstract protected function getCacheTag(): string;

    /**
     * @return Repository
     */
    abstract protected function getCache(): Repository;
}
