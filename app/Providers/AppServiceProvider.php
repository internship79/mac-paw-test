<?php

namespace App\Providers;

use App\Repositories\Neo\NeoRepository;
use App\Repositories\Neo\NeoRepositoryCaching;
use App\Repositories\Neo\NeoRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(NeoRepositoryInterface::class,
        NeoRepositoryCaching::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      //
    }
}
