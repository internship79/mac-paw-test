<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NeoFastestRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'hazardous' => 'sometimes|in:true,false|string'
        ];
    }
}
