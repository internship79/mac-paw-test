<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NeoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'referenced' => $this->resource->referenced,
            'name' => $this->resource->name,
            'speed' => $this->resource->speed,
            'is_hazardous' => $this->resource->is_hazardous,
            'date' => $this->resource->date,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
