<?php

namespace App\Http\Controllers;

use App\Http\Requests\NeoFastestRequest;
use App\Http\Resources\NeoResource;
use App\Service\NeoService;
use Illuminate\Http\JsonResponse;

class NeoController extends Controller
{
    /**
     * @param NeoService $service
     */
    public function __construct(private readonly NeoService $service) {}

    /**
     * @return JsonResponse
     */
    public function allHazardous():JsonResponse
    {
        return NeoResource::collection($this->service->getAllHazardous())
            ->response()
            ->setStatusCode(JsonResponse::HTTP_OK);
    }

    /**
     * @param NeoFastestRequest $request
     * @return JsonResponse
     */
    public function getFastest(NeoFastestRequest $request): JsonResponse
    {
        $hazardous = $request->query('hazardous', 'false');
        return NeoResource::collection($this->service->getFastest($hazardous))
            ->response()
            ->setStatusCode(JsonResponse::HTTP_OK);
    }
}
