<?php

namespace App\Http\Controllers;

use App\Http\Resources\WelcomeResource;
use App\Service\WelcomeService;
use Illuminate\Http\JsonResponse;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(WelcomeService $service): JsonResponse
    {
        return WelcomeResource::make($service->welcome())
            ->response()
            ->setStatusCode(JsonResponse::HTTP_OK);
    }

}
