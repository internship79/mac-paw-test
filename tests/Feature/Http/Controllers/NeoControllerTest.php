<?php

namespace Tests\Feature\Http\Controllers;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NeoControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function test_neos_are_hazardous_()
    {
        $response = $this->get('/neo/hazardous');

        $response->assertStatus(200)
            ->assertJsonFragment(['is_hazardous' => 1])
            ->assertJsonStructure($this->responseStructure());
    }

    /**
     * @return void
     */
    public function test_neo_fastest_hazardous_true()
    {
        $response = $this->get('/neo/fastest?hazardous=true');

        $response->assertStatus(200)
            ->assertJsonFragment(['is_hazardous' => 1])
            ->assertJsonStructure($this->responseStructure());
    }

    /**
     * @return void
     */
    public function test_neo_fastest_hazardous_false()
    {
        $response = $this->get('/neo/fastest?hazardous=false');

        $response->assertStatus(200)
            ->assertJsonFragment(['is_hazardous' => 0])
            ->assertJsonStructure($this->responseStructure());
    }

    /**
     * @return void
     */
    public function test_neo_fastest_hazardous_default_false()
    {
        $response = $this->get('/neo/fastest');

        $response->assertStatus(200)
            ->assertJsonFragment(['is_hazardous' => 0])
            ->assertJsonStructure($this->responseStructure());
    }

    /**
     * @return array
     */
    private function responseStructure(): array
    {
        return [
            'data' => [
                '*' => [
                    'referenced' ,
                    'name' ,
                    'speed' ,
                    'is_hazardous' ,
                    'date',
                    'created_at' ,
                    'updated_at'
                ]
            ]
        ];
    }
}
