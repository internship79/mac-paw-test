<?php

namespace Tests\Feature\Http\Controllers;

use App\Service\WelcomeService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class WelcomeControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function test_main_endpoint()
    {
        $service =  \Mockery::mock(WelcomeService::class);
        app()->instance(WelcomeService::class, $service);

        $service->shouldReceive('welcome')->once();

        $response = $this->get('/');

        $response
            ->assertStatus(200)
            ->assertJson(fn (AssertableJson $json) =>
                $json->has('hello'));
    }
}
