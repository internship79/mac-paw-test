<?php

namespace Tests\Feature\Console\Commands\Neo;

use App\Service\NeoService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SyncTest extends TestCase
{
    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->service = \Mockery::mock(NeoService::class);
        app()->instance(NeoService::class, $this->service);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_sync()
    {
        $this->service->shouldReceive('syncWithNasaNeoFeed')->once();

        $this->artisan('neo:sync')
            ->assertSuccessful();
    }
}
