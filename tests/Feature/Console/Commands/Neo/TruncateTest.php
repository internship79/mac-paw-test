<?php

namespace Tests\Feature\Console\Commands\Neo;

use App\Service\NeoService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery\Mock;
use Tests\TestCase;

class TruncateTest extends TestCase
{
    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->service = \Mockery::mock(NeoService::class);
        app()->instance(NeoService::class, $this->service);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_confirm_truncate()
    {
        $this->service->shouldReceive('truncate')
            ->once();

        $this->artisan('neo:truncate')
            ->expectsConfirmation('Do you wish to truncate near_earth_objects_table?', 'yes')
            ->assertSuccessful();
    }

    /**
     * @return void
     */
    public function test_reject_truncate()
    {
        $this->artisan('neo:truncate')
            ->expectsConfirmation('Do you wish to truncate near_earth_objects_table?')
            ->assertFailed();
    }
}
