# PET-Project

test task for Internship 2022

## Tasks

1) Create main endpoint with proper json response: Hell: Internship.
2) Make Model NearEarthObject: referenced, name, speed, is_hazardous, date.
3) Create endpoint neo/hazardous. Display all hazardous object (json format).
4) Create endpoint neo/fastest?hazardous=true or false. Display objects sorted by speed, default hazardous value must be false.
5) Get information about near earth objects for the last three days (https://api.nasa.gov) and store it into database.

## Improvements
1) Use Decorator pattern: Implement caching of GET requests.
2) Jobs: Inform user about changes in database.
