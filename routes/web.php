<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index']);

Route::get('/neo/hazardous', [\App\Http\Controllers\NeoController::class, 'allHazardous']);

Route::get('/neo/fastest', [\App\Http\Controllers\NeoController::class, 'getFastest']);

Route::get('/neos', function () {
    \Illuminate\Support\Facades\Notification::route('telegram', '270563806')
        ->notify(new \App\Notifications\Telegram\NeoSyncNotification());
});

