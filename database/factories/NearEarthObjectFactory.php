<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NearEarthObjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'referenced' => $this->faker->unique()->randomNumber(),
            'name' => $this->faker->name(),
            'speed' => $this->faker->randomFloat(),
            'is_hazardous' => $this->faker->boolean(60),
            'date' => $this->faker->date(),
        ];
    }
}
